﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfBounds : MonoBehaviour {

	private void OnTriggerEnter(Collider ball)
	{
        ball.GetComponent<Ball>().outside = true;
	}
}
