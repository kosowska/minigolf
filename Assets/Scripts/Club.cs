﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Club", menuName = "Golf/Club", order = 0)]
public class Club : ScriptableObject {

    public float maxPower = 2;
    public float angle = 45;

    public Vector3 GetImpulse(float n)
    {
        return Vector3.zero;
    }
}
