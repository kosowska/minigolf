﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour {
    public int value;
    public bool isAvailable = false;
    public float unavailableTime;
    private float startTime = 0;
    public void Block()
    {
        startTime = Time.time;
        isAvailable = false;
    }
	// Use this for initialization

	private void Update()
	{
        if(Time.time - startTime > unavailableTime){
            isAvailable = true;
        }
	}
	void OnTriggerEnter(Collider other)
    {
        if (!isAvailable) return;
        other.GetComponent<AudioSource>().PlayOneShot(other.GetComponent<Ball>().pointSound, 1f);
        other.GetComponent<Ball>().score += value;
        gameObject.SetActive(false);
        //GameObject.Destroy(gameObject);
    }
}
