﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedingPanel : MonoBehaviour {
	
	public float forceRate;
	// Use this for initialization
	public void OnTriggerEnter(Collider ball)
	{
		Vector3 direction = -Vector3.right;
		direction = Quaternion.AngleAxis(transform.eulerAngles.y, Vector3.up) * direction;
		Vector3 force = direction*forceRate;
		ball.GetComponent<Rigidbody>().AddForce(force,ForceMode.Impulse);
	}
}
