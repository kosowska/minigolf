﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClubChoose : MonoBehaviour {
    public Club sandClub;
    public Club grassClub;
    public Ball ball;
    public Vector3 GetForce(Vector3 direction, float pullScale){
        Club club = ball.bed.GetComponent<SurfaceType>().surface.ChooseClub();
        Debug.Log(club.angle);
        Vector3 perpedicular = Quaternion.Euler(0, 90, 0) * direction;
        Vector3 newDirection = Quaternion.AngleAxis(club.angle, perpedicular) * direction;
        return newDirection * pullScale * club.maxPower;
    }
	private void Start()
	{
        ball = GetComponent<Ball>();
	}
}
