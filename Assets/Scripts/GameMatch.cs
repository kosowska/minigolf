﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using UnityEngine.SceneManagement;
using System.Linq;


public class GameMatch : MonoBehaviour {

    public Button overButton;//przycisk do przejścia na następny poziom
    public static float[] stars; // ile gwiazdek mamy w konkretnych poziomach
    public int levelNumber;// na którym poziomie aktualnie jesteśmy 
    public GameObject startPosition;//pozycja w której piłka znajduje się na początku 
    public GameObject ballPrefab;
    public AudioClip cheering;//dźwięk w momencie wrzucenia piłki do dołka
    public AudioClip song;
    public GameObject[] balls = new GameObject[2];
    public Text countText1;
     public Text winner;

     private float songStartTime;
  //  public Text timer;
   // public Text fPlayer;
    [HideInInspector]
    public List<Surface> hierarchy = new List<Surface>();// które powierzchnie są ważniejsze

    public string[] names;

    int ballCounter;

    public Ball CurrentPlayerBall => balls[currentPlayer] == null ? null : balls[currentPlayer].GetComponent<Ball>();

   // int playersNum;
    bool isTutorial;
    public float minDistance;
    Hole hole;
    int currentPlayer;
    int firstPlayer;
    int secondPlayer;

    float startTime;
    bool gameOver = false;
    public float turnTime;
    bool isEnd;
    float timer1, timer2;
    string whoWin;
    float thisTurnTime;
    Vector3 windDirection;
    float windForce;
   private void GetWind()
   {
       windDirection = new Vector3(Random.Range(0f,1f),0, Random.Range(0f,1f)); // 0,0,1 o kat (0,360)
       windForce = Random.Range(0f, 0.1f);

   }

   public void MainMenuButton()
   {
       SceneManager.LoadScene("Levels");
   }
  public void UseWind()
   {
       CurrentPlayerBall.GetComponent<Rigidbody>().AddForce(windDirection * windForce, ForceMode.VelocityChange);
   }
    public bool TimesUp()
    {
        return (Time.time - startTime >= turnTime);
    }
    bool OB()
    {
        return balls[currentPlayer].GetComponent<Ball>().outside;
    }
    bool AllStable()
    {
        bool stable = true;

        for (int i = 0; i < balls.Length; i ++)
        {
            
            if (balls[i] != null)
            {
                var ball = balls[i].GetComponent<Ball>();
                stable = stable && (ball.AtRest() || ball.outside);
            }
                
        }
        return stable;
    }
    void StopSlowBalls()
    {
        for (int i = 0; i < balls.Length; i++)
        {
            if (balls[i] != null && (balls[i].GetComponent<Ball>().IsSlow() || balls[i].GetComponent<Ball>().outside)) balls[i].GetComponent<Ball>().Stop();
        }

    }
    void NextTurn()
    {
        
        //timer.color = Color.blue;
        // thisTurnTime = turnTime;
        //timer.text = thisTurnTime.ToString();
      //  timer1 = Time.time;
       // timer2 = Time.time;
        //currentPlayer = ((currentPlayer == 1) ? 0 : 1) % playersNum; // lub (player+1)%2; player ^ 1;
        startTime = Time.time;
        if(balls[currentPlayer] == null)
        {
            balls[currentPlayer] = GameObject.Instantiate(ballPrefab, startPosition.transform.position, Quaternion.identity);
            balls[currentPlayer].GetComponent<Ball>().myNumber = ballCounter++;
           balls[currentPlayer].GetComponent<Ball>().validPosition = balls[currentPlayer].transform.position;
        }


        balls[currentPlayer].GetComponent<Ball>().SetPosition();

        balls[currentPlayer].GetComponent<Ball>().validPosition = balls[currentPlayer].GetComponent<Ball>().transform.position;
        CurrentPlayerBall.isMoved = false;
     //   GetWind(); // We get a new wind
      //  Debug.Log("WE GOT NEW WIND!");
    }
     
    void WhoWin(out bool isEnd, out string whoWin)
    {
        whoWin = names[0];
        isEnd = false;
        if(balls[currentPlayer].GetComponent<Ball>().isInHole == true && !CurrentPlayerBall.isMoved) {
            isEnd = true;
            if (levelNumber == 4)
            {
                winner.text = "Game Over";
                winner.gameObject.SetActive(true);
                gameOver = true;
            }
            else
            {
                if (isTutorial == false) overButton.gameObject.SetActive(true);
               
                GetComponent<AudioSource>().PlayOneShot(cheering, 0.5f);
            }
            if (levelNumber < stars.Length && CurrentPlayerBall.score > stars[levelNumber])
            {
                GetComponent<Save>().SaveScore(CurrentPlayerBall.score, levelNumber);
            }
        }
       
        /* whoWin = names[0];
         if (balls[firstPlayer] == null || balls[secondPlayer] == null) return;
         if(balls[currentPlayer].GetComponent<Ball>().isInHole == true && !CurrentPlayerBall.isMoved) {
             whoWin = names[currentPlayer];
             isEnd = true;
             return;
         }
         if(balls[secondPlayer].GetComponent<Ball>().isInHole == true && balls[firstPlayer].GetComponent<Ball>().isInHole == false){
            
             isEnd = true;
             whoWin = names[1];
             return;
         }
         if(balls[firstPlayer].GetComponent<Ball>().isInHole == true && balls[secondPlayer].GetComponent<Ball>().isInHole == true){
           
             isEnd = true;
             if (balls[firstPlayer].GetComponent<Ball>().score > balls[secondPlayer].GetComponent<Ball>().score) whoWin = names[0];
             else if (balls[firstPlayer].GetComponent<Ball>().score < balls[secondPlayer].GetComponent<Ball>().score) whoWin = names[1];
             else whoWin = "DRAW";
         }*/

    }
	// Use this for initialization
    void Start (){
        
        overButton.gameObject.SetActive(false);
        levelNumber = SceneManager.GetActiveScene().buildIndex - 3;
        if(levelNumber>=stars.Length) isTutorial = true;
        else isTutorial = false;
        Debug.Log("IS TUTORIAL");
        Debug.Log(isTutorial);
        Debug.Log("lvlnmb");
        Debug.Log( levelNumber);
        Debug.Log(stars.Length);
        startPosition = FindObjectOfType<StartPosition>().gameObject;
        //playersNum = names.Length;
        firstPlayer = 0;
        secondPlayer = 1;
        thisTurnTime = turnTime;
        winner.gameObject.SetActive(false);
        currentPlayer = 0;

        /*if (playersNum > 1)
        {
            //  NextTurn();
           // if (firstPlayer == 0) fPlayer.text = names[0] + "starts!";
           // else fPlayer.text = names[1] + "starts!";
           // Destroy(fPlayer, 2.0f);
            countText1.text = "Points" + "0";
           // countText2.text = names[1] + "0";
            winner.text = "";
           // timer.text = turnTime.ToString();
           // timer1 = Time.time;
          //  timer2 = Time.time;
            thisTurnTime = turnTime;
        }
        else
        {
           // Destroy(fPlayer, 0f);
           // Destroy(countText1, 0f);
           // Destroy(countText2, 0f);
           // Destroy(winner, 0f);
            winner.gameObject.SetActive(false);
            //Destroy(timer, 0f);
            secondPlayer = 0;
            currentPlayer = 0;
        }*/
        while(AllStable() == false);
        NextTurn();
   
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Time.time - songStartTime >= song.length || songStartTime == 0)
        {
            GetComponent<AudioSource>().PlayOneShot(song, 0.2f);
            songStartTime = Time.time;
        }
      // CurrentPlayerBall = balls[currentPlayer] == null ? null : balls[currentPlayer].GetComponent<Ball>();
         //CurrentPlayerBall = balls[currentPlayer] == null ? null : balls[currentPlayer].GetComponent<Ball>();
        if(balls[0] != null) countText1.text = names[0] + ": " + balls[0].GetComponent<Ball>().score.ToString();
        //if(balls[1] != null) countText2.text = names[1] + balls[1].GetComponent<Ball>().score.ToString();
       
     //   Debug.Log("podloze");
   //    Debug.Log(CurrentPlayerBall.bed);
       if (isEnd == false)
       { 
           WhoWin(out isEnd, out whoWin);
           
       }
       /* if (timer2 - timer1 >= 1.0 && CurrentPlayerBall.isMoved == false)
        {
            
            WhoWin(out isEnd, out whoWin);
            timer1 = Time.time;
            if(thisTurnTime > 0) thisTurnTime -= 1;
            timer.text = thisTurnTime.ToString();
        }*/

       /* if (isEnd && !gameOver)
        {
            if (isTutorial == false) overButton.gameObject.SetActive(true);
            if (levelNumber < stars.Length && thisTurnTime > stars[levelNumber])
            {
                GetComponent<Save>().SaveScore(thisTurnTime, levelNumber);
            }

            GetComponent<AudioSource>().PlayOneShot(cheering, 1f);
        }*/
        if(!isEnd)
        {
            StopSlowBalls();
            if(CurrentPlayerBall.GetComponent<Rigidbody>().velocity.magnitude > 2) UseWind();
            if (!isEnd && AllStable() && (CurrentPlayerBall.isMoved || TimesUp() || OB()))
            {
               
                WhoWin(out isEnd, out whoWin);
                if(!isEnd) NextTurn();

            }
        }
        timer2 = Time.time;
       /* if(thisTurnTime <= 5){
            timer.color = Color.red;
        }*/
        //if(CurrentPlayerBall.GetComponent<Ball>().isMoved == true) UseWind();
	}

    static GameMatch _instance;

    public static GameMatch Instance {
        get {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameMatch>();
            }
            return _instance;
        }
    }
}
