﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobFlight : MonoBehaviour {
    public AnimationCurve curve;   
    Vector3 startPosition;
    Vector3 endPosition;
    float startTime;
    public float animationTime = 0.5f;
	// Use this for initialization
	void Start () {
        startPosition = this.transform.position;
        endPosition = Quaternion.Euler(0, Random.Range(0, 360), 0) * Vector3.forward * Random.Range(1, 2) + startPosition;
        startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        if(Time.time - startTime >= animationTime){
            transform.position = endPosition;
            Destroy(this);
        }
        else{
            float normTime = (Time.time - startTime) / animationTime;
            transform.position = Vector3.Lerp(startPosition, endPosition, normTime) + (new Vector3(0, curve.Evaluate(normTime), 0));
        }
	}
}
