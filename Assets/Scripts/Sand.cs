﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sand : MonoBehaviour {
    float startDrag, startAngularDrag;
    void OnTriggerEnter(Collider other){
        startDrag = other.GetComponent<Rigidbody>().drag;
        startAngularDrag = other.GetComponent<Rigidbody>().angularDrag;
        other.GetComponent<Rigidbody>().drag = 2;
        other.GetComponent<Rigidbody>().angularDrag = 2;
        //  other.GetComponent<Ball>().surface = "Sand";
        other.GetComponent<Ball>().bed = this.gameObject;
    }

}
