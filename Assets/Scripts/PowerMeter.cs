﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class PowerMeter : MonoBehaviour {

    public GameObject outsideSphere;
    public GameObject insideSphere;

    public GameObject arrow;

    private Vector3 initialMousePosition;
    Ray ray;
    Vector3 hitPoint;
    Vector3 direction;
    Plane groundPlane;
    float d;
    public float maxPull = 200;
    public float minInsideSphere;
    bool isPulling = false;
    float pullScale;
    float lastPullScale;
	void Start () {
        outsideSphere.GetComponent<Renderer>().enabled = false;
        insideSphere.GetComponent<Renderer>().enabled = false;
        transform.localScale = new Vector3(1,1,1);
	}
	
	void Update () {
        
        var ball = GameMatch.Instance.CurrentPlayerBall;
        if (ball.GetComponent<Ball>().isMoved == false)
        {
            var cam = Camera.main;
            if (Input.GetButtonDown("Fire1"))
            { 
                initialMousePosition = Input.mousePosition;
                if ((Input.mousePosition - cam.WorldToScreenPoint(ball.transform.position)).magnitude < GameMatch.Instance.minDistance)
                {
                  //   transform.localScale = new Vector3(1,1,1) * CameraTarget.Instance.zoomChange * (float)0.2;
                  
                    transform.position = ball.transform.position;
                    outsideSphere.GetComponent<Renderer>().enabled = true;
                    insideSphere.GetComponent<Renderer>().enabled = true;
                    arrow.active = true;

                    isPulling = true;
                }

            }
            if (isPulling)
            {
                pullScale = Mathf.Clamp01((Input.mousePosition - initialMousePosition).magnitude / maxPull);
                insideSphere.transform.localScale = pullScale * Vector3.one;
                arrow.transform.Rotate(0,0, lastPullScale-pullScale);
                ray = cam.ScreenPointToRay(Input.mousePosition);
                Plane groundPlane = new Plane(new Vector3(0, 1, 0), ball.transform.position);
                if (groundPlane.Raycast(ray, out d))
                {
                    hitPoint = ray.GetPoint(d);
                    direction = (ball.transform.position - hitPoint).normalized ;

                    Club club = ball.bed.GetComponent<SurfaceType>().surface.ChooseClub();
                    arrow.transform.rotation = Quaternion.LookRotation(direction, Vector3.up);
                    arrow.transform.Rotate(-club.angle,0,0);
                }

                if(GameMatch.Instance.TimesUp()){
                    outsideSphere.GetComponent<Renderer>().enabled = false;
                    insideSphere.GetComponent<Renderer>().enabled = false;
                    arrow.active = false;
                    isPulling = false;
                }
                if (Input.GetButtonUp("Fire1"))
                {

                    outsideSphere.GetComponent<Renderer>().enabled = false;
                    insideSphere.GetComponent<Renderer>().enabled = false;
                    arrow.active = false;

                    isPulling = false;
                    if (insideSphere.transform.localScale.magnitude > minInsideSphere)
                    {
                        ball.GetComponent<Ball>().isMoved = true;
                        ball.GetComponent<Ball>().startTime = 0;
                        ray = cam.ScreenPointToRay(Input.mousePosition);
                        groundPlane = new Plane(new Vector3(0, 1, 0), ball.transform.position);

                         d = 0;
                        if (groundPlane.Raycast(ray, out d))
                        {
                            hitPoint = ray.GetPoint(d);
                            direction = (ball.transform.position - hitPoint).normalized;

                            ball.Shot(direction, pullScale);

                        }
                    }
                }
                lastPullScale = pullScale;
            }
        }
           
	}
}
