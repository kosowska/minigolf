﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {
    public GameObject matchingTeleport;
    public bool isFree = true;
    public int id;
    void Start()
    {
        isFree = true;
    }
    void OnTriggerEnter(Collider ball)
	{

        // if (surface != ("Teleport" + id.ToString()))
        if (ball.GetComponent<Ball>().cantBeTeleported == false)
        {
            // Debug.Log("Teleportujemy");
            ball.GetComponent<Ball>().cantBeTeleported = true;
            ball.GetComponent<Ball>().bed = matchingTeleport.gameObject;
            ball.GetComponent<AudioSource>().PlayOneShot(ball.GetComponent<Ball>().teleportSound, 1f);
            ball.GetComponent<Ball>().Stop();
            ball.GetComponent<Ball>().transform.position = matchingTeleport.transform.position;
        }


	}

}
