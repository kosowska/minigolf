﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Save : MonoBehaviour {

	// Use this for initialization
	public void SaveScore(float score, int indx)
	{
	//	Debug.Log("ZAPISUJE");
	//	Debug.Log(score);
		GameMatch.stars[indx] = score;
		PlayerPrefs.SetFloat("BestScore" + indx, score);
	}
	public void LoadScore(int howManyLevels)
	{
		for(int i=0;i<howManyLevels;i++)
		{
			GameMatch.stars[i] = PlayerPrefs.GetFloat("BestScore"+i);
		}
		//GameMatch.bestTime = PlayerPrefs.GetFloat("BestScore");
	}
}
