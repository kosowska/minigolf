﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu()]
public class Surface : ScriptableObject
{
    [SerializeField]
    private bool isStopable;
    [SerializeField]
    private bool isTeleport;
    public Club club; 
    public bool CanStop; //=> isStopable;
    public string surfaceString;
    public bool IsTeleport => isTeleport;
    public Club ChooseClub()
    {
        return club;
    }
    void Update()
    {
        CanStop = isStopable;
    }
    //[SerializeField] // prywatne ale w inspektorze
}
