﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTarget : MonoBehaviour {
    Vector3 initialMousePosition;
    bool isPulling;
    Vector3 direction;
    bool zoom = false;
    public float change;
    Vector3 lastMousePosition;
    public float maxChange;
    public float zoomScale;
    public float zoomChange;
    public float minDistance;
    public float maxDistance;
    public float rotateScale;
    public GameObject match;
    bool changeChosen;
    GameObject ball;
    Vector3 ray;
	// Use this for initialization
	void Start () {
        zoomChange = 5;

	}
	
	// Update is called once per frame
	void Update () {
    if (GameMatch.Instance.CurrentPlayerBall != null) transform.position = GameMatch.Instance.CurrentPlayerBall.transform.position;

        var cam = Camera.main;
            
        if (Input.GetButtonDown("Fire1"))
        { // przycisk został wciśnięty w danej klatce (wystarczy raz) 
            initialMousePosition = Input.mousePosition;
            if (((Input.mousePosition - cam.WorldToScreenPoint(GameMatch.Instance.CurrentPlayerBall.transform.position)).magnitude) > GameMatch.Instance.minDistance)
            {
                
                isPulling = true;
            }

        }
        else if (isPulling)
        {

            var initialDirection = Input.mousePosition - initialMousePosition;
            direction = Input.mousePosition - lastMousePosition;
            change = (Input.mousePosition - lastMousePosition).magnitude / maxChange;
            if (initialDirection.magnitude > 0.06f * Screen.width || changeChosen)
            {
                if (changeChosen == false)
                {
                    if (Mathf.Abs(Vector3.Dot(initialDirection, Vector3.right)) >= Mathf.Abs(Vector3.Dot(initialDirection, Vector3.up)))
                    {
                        zoom = false;
                    }
                    else zoom = true;
                    changeChosen = true;
                }
                if (zoom)
                {
     
                    ball = match.GetComponent<GameMatch>().CurrentPlayerBall.gameObject;
                    change *= - Mathf.Sign(direction.y) * zoomScale;
                    ray = (transform.position - cam.transform.position) * change;
                    if(((cam.transform.position + ray) - ball.transform.position).magnitude > minDistance  
                       && ((cam.transform.position + ray) - ball.transform.position).magnitude < maxDistance)
                    {
                        cam.transform.position += ray;
                    }
                  //  zoomChange = cam.transform.position.magnitude * 0.1f;
                    //zoomChange = - (float)Mathf.Sign(direction.y) * ray.magnitude * 0.5f;
                    //zoomChange = 
                   // Debug.Log("zoom:");
                  //  Debug.Log(zoomChange);
                }
                else
                {
                    change *= Mathf.Sign(direction.x) * rotateScale;
                    transform.Rotate(0, change, 0);
                }
            }
            if (Input.GetButtonUp("Fire1"))
            {
                isPulling = false;
                zoom = false;
                changeChosen = false;
            }
           // if (zoom == false) Debug.Log("falsz!");
        }
        lastMousePosition = Input.mousePosition;
	}
    static CameraTarget _instance;

    public static CameraTarget Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<CameraTarget>();
            }
            return _instance;
        }
    }
}
