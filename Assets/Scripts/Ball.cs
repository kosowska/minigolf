﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

public class Ball : MonoBehaviour {
    public GameObject diamond;
    public AnimationCurve curve;
    //DŹWIĘKI 
    public AudioClip shoting;
    public AudioClip bombSound;
    public AudioClip pointSound;
    public AudioClip trampolineSound;
    public AudioClip teleportSound;
    public AudioSource source;
    public GameObject match;
    Component gameComponent;

    public List<Surface> hierarchy;
    public bool cantBeTeleported = false;
    public float defaultDrag;
    public float defaultAngularDrag;
    public Surface surface; //na jakiej powierzchni znajduje się piłka 
    public Surface decorationSurface;
    public GameObject bed;
    public GameObject previousBed;
    public int myNumber; 
    public float impulsePower = 5; // moc wybicia piłki
    public float minVelocity = 2; // minimalna prędkość z jaką może się toczyć
    public bool isMoved = false; //czy jest poruszona
    public float startTime = 0; //kiedy została poruszona
    public bool isInHole = false; //czy jest już w dołku
    public int score; //ile diamencików zebrał
    public bool outside; // czy wypadła za planszę
    public Vector3 validPosition; // ostatnia dobra pozycja
   
    //public Text countText;
    public void Shot(Vector3 direction, float pullPower) // strzelenie piłką 
    {
        
        source.PlayOneShot(shoting,1f);
        Vector3 force = GetComponent<ClubChoose>().GetForce(direction, pullPower);
        GetComponent<Rigidbody>().AddForce(force, ForceMode.Impulse);
        //match.GetComponent<GameMatch>().UseWind();
    }
    public void LosePoints(int howMany) //odejmij piłce punkty (w przypadku uderzenia w bombę)
    {
        GameObject d;
        for (int i = 0; i < howMany; i++)
        {
            d = GameObject.Instantiate(diamond, transform.position, Quaternion.identity);
            d.AddComponent<LobFlight>();
            d.GetComponent<Point>().Block();
            d.GetComponent<LobFlight>().curve = curve;
        }
        score -= howMany;
    }
    public bool AtRest() // czy piłka jest w spoczynku
    {
         return GetComponent<Rigidbody>().IsSleeping();
    }
    public void SetPosition() //ustaw poprawną pozycję 
    {
        if(outside){
            transform.position = validPosition;
            cantBeTeleported = true;
            outside = false;
            startTime = 0;
        }

    }
    public void Stop() //zatrzymaj piłkę
    {
        GetComponent<Rigidbody>().Sleep();
    }
    public bool IsSlow() //czy piłka jest za wolna
    {
        
       // Debug.Log(bed.GetComponent<SurfaceType>().surface);
        if(bed != null  && bed.GetComponent<SurfaceType>().surface.CanStop && (GetComponent<Rigidbody>().velocity.magnitude < minVelocity) && startTime > 1.5f){
            Debug.Log("IS STOPPED!");
          //  Debug.Log(startTime);
           // Debug.Log(startTime);
        }
         // Debug.Log(bed.GetComponent<SurfaceType>().surface.CanStop && ((GetComponent<Rigidbody>().velocity.magnitude < minVelocity) && (Time.time - startTime) > 2));
         return bed != null  && bed.GetComponent<SurfaceType>().surface.CanStop && ((GetComponent<Rigidbody>().velocity.magnitude < minVelocity) && startTime > 1.5f);
      //  return false;
    }
    // Use this for initialization
    void Start()
    {
        startTime = 0;
        score = 0;
        //Debug.Log("HIERARCHIA:");
        source = GetComponent<AudioSource>();
        hierarchy = match.GetComponent<GameMatch>().hierarchy;
        bed = FindObjectOfType<Grass>().gameObject;

    }
	private void Update()
	{
        startTime+=0.01f;
        if (bed == null)
        {
            bed = FindObjectOfType<Grass>().gameObject;
        }
       
        //Debug.Log("Aktualne podloze:");
        //Debug.Log(bed);
        Collider[] colliders = Physics.OverlapSphere(transform.position, GetComponent<SphereCollider>().radius * transform.localScale.x+0.01f);
        Debug.Log("COLLIDERY!");
        Debug.Log(colliders.Length);
        foreach (var col in colliders)
        {
            
            if (col.GetComponent<SurfaceType>() == null)
            {
                col.gameObject.AddComponent(typeof(SurfaceType));
                col.GetComponent<SurfaceType>().surface = decorationSurface;

            }
            Debug.Log(col.GetComponent<SurfaceType>().surface);
        }
        Debug.Log("COLLIDERY!");
        Debug.Log(colliders.Length);
        if(colliders.Length>0) {
            //Debug.Log(colliders[0]);
            Array.Sort(colliders, (a, b) => (hierarchy.IndexOf(a.GetComponent<SurfaceType>().surface).CompareTo(hierarchy.IndexOf(b.GetComponent<SurfaceType>().surface))));
        }
      //  bed = colliders[0].gameObject;

        foreach (var c in colliders)
        {
            if (c.GetComponent<SurfaceType>().surface != null)
            {
                Debug.Log(c.gameObject);
                if(bed.GetComponent<SurfaceType>().surface.IsTeleport == true){
                    Debug.Log("JESTEMY NA TELEPORT");
                    //bed.GetComponent<Teleport>().isFree = true;
                    if (c.gameObject != bed)
                    {
                        
                        cantBeTeleported = false;
                    }
                    else
                    {
                        Debug.Log("JESTESMY TAM");
                        Debug.Log(c);
                        Debug.Log((bed));
                    }
                }
               // Debug.Log(c.GetComponent<SurfaceType>().surface);
                bed = c.gameObject;
                break;
            }
        }
       
        /*if(bed!= null && bed.GetComponent<SurfaceType>().surface.IsTeleport == false)
        {
               cantBeTeleported = false;
        }
        else if(bed != null){
            bed.GetComponent<Teleport>().isFree = false;
        }*/



	}
}
