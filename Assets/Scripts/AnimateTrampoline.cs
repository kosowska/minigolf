﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateTrampoline : MonoBehaviour {

	public AnimationCurve curve;

    public float startTime;
    public float animationTime = 0.5f;
    Vector3 startScale;
	// Use this for initialization
	void Start () {
        startScale = transform.localScale;
        
	}
	
	// Update is called once per frame
	void Update () {
           float normTime = (Time.time - startTime) / animationTime;
           transform.localScale = new Vector3(curve.Evaluate(normTime), curve.Evaluate(normTime), curve.Evaluate(normTime));
        }
	//}
}
