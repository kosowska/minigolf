﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour {
    public float strikeForce;
    public int diamondLost;
   // public AudioClip bombSound;
  //  private AudioSource source;
    private float volLowRange = 500f;
    private float volHighRange = 1000f;

	public void OnTriggerEnter(Collider ball)
    {
        var ballComponent = ball.GetComponent<Ball>();
        ball.GetComponent<AudioSource>().PlayOneShot(ballComponent.bombSound, 1f);
        var direction = ball.GetComponent<Rigidbody>().velocity.normalized; //local direction
      
        ball.GetComponent<Rigidbody>().AddForce(strikeForce * (-direction + new Vector3(0,0.7f,0)).normalized, ForceMode.Impulse);

        ballComponent.LosePoints(Mathf.Min(ballComponent.score, diamondLost));
       // Debug.Log("DUPA");
        GameObject.Destroy(gameObject);
	}
}
