﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(GameMatch), true, isFallback = true)]
public class GameMatchEditor : Editor
  {
    
    private ReorderableList _myList;
 
    public void OnEnable ()
    {
        
        _myList = new ReorderableList (serializedObject, serializedObject.FindProperty ("hierarchy"), true, true, true, true);
    
        _myList.drawHeaderCallback = rect => {
            EditorGUI.LabelField (rect, "My Reorderable List", EditorStyles.boldLabel);
        };
 
        _myList.drawElementCallback = 
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = _myList.serializedProperty.GetArrayElementAtIndex (index);
            EditorGUI.ObjectField (new Rect (rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), element, GUIContent.none);
        };
    }
 
    public override void OnInspectorGUI ()
    {
        base.OnInspectorGUI();
        serializedObject.Update ();
        _myList.DoLayoutList ();
        serializedObject.ApplyModifiedProperties ();
    }
 
}*/
using UnityEditor;
//make sure to add the unityEditorInternal namespace
using UnityEditorInternal;
using UnityEngine;
 
[CustomEditor(typeof(GameMatch), true, isFallback = true)]
public class myClassEditor : Editor
{
    GameMatch _target;

    private ReorderableList _myList;

    public void OnEnable()
    {

        _target = (GameMatch)target;

        _myList = new ReorderableList(serializedObject, serializedObject.FindProperty("hierarchy"), true, true, true, true);

        _myList.drawHeaderCallback = rect => {
            EditorGUI.LabelField(rect, "My Reorderable List", EditorStyles.boldLabel);
        };

        _myList.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) => {
                var element = _myList.serializedProperty.GetArrayElementAtIndex(index);
                EditorGUI.ObjectField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), element, GUIContent.none);
            };
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.Update();
        _myList.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
    }

}