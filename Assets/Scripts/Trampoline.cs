﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampoline : MonoBehaviour {

    float startTime;
   public  GameObject trampoline;
    public float strikeForce;
    public float delta;
	// Use this for initialization

    
	public void OnCollisionEnter(Collision collision)
	{
        Debug.Log("UDERZONE!!!");
        startTime = Time.time;
        var ball = collision.gameObject;
        trampoline.GetComponent<AnimateTrampoline>().startTime = Time.time;
        var direction = ball.GetComponent<Rigidbody>().velocity.normalized;
        float zDelta = direction.z * delta ;
        float xDelta = direction.x  * delta;
        ball.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        ball.GetComponent<Rigidbody>().AddForce(strikeForce * new Vector3(xDelta, 0.4f, zDelta), ForceMode.Impulse);
        var ballComponent = ball.GetComponent<Ball>();

        ballComponent.bed = this.gameObject;
        ball.GetComponent<Rigidbody>().drag = ball.GetComponent<Ball>().defaultDrag;
        ball.GetComponent<Rigidbody>().angularDrag =  ball.GetComponent<Ball>().defaultAngularDrag;
        ball.GetComponent<AudioSource>().PlayOneShot(ballComponent.trampolineSound, 1f);
	}
}
