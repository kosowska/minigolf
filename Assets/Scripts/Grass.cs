﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass : MonoBehaviour {
    public void OnCollisionEnter(Collision collision)
    {
        Debug.Log("ZMIANA");
        //var other = collision.gameObject;
        collision.gameObject.GetComponent<Rigidbody>().drag = collision.gameObject.GetComponent<Ball>().defaultDrag;
        collision.gameObject.GetComponent<Rigidbody>().angularDrag = collision.gameObject.GetComponent<Ball>().defaultAngularDrag;
        //  collision.gameObject.GetComponent<Ball>().surface = "Grass";
        collision.gameObject.GetComponent<Ball>().bed = gameObject;

    }
}
