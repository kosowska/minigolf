﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Levels : MonoBehaviour
{
    public AudioClip song;
    public AudioSource source;
    public Image[] levelStars;
    public Sprite zeroStars;
    public Sprite oneStar;
    public Sprite twoStars;
    public Sprite threeStars;
    int howManyLevels;
    public void ShowStars()
    {

        Debug.Log(howManyLevels);
        for(int i=0;i<howManyLevels;i++)
        {
            //int diamondsOnLevel = .FindObjectsOfType<Point>().Length;
              //Debug.Log("diamnety na planszy");
              // Debug.Log(diamondsOnLevel);
               if(GameMatch.stars[i]> 25){
                    levelStars[i].sprite = threeStars;
                }
                else if(GameMatch.stars[i]> 15){
                    levelStars[i].sprite = twoStars;
                }
                else if(GameMatch.stars[i]>=1){
                    levelStars[i].sprite = oneStar;
                }
                else levelStars[i].sprite = zeroStars;
            

        }
    }
    public void ResetLevels()
    {
        for(int i=0;i<levelStars.Length;i++)
        {
            GetComponent<Save>().SaveScore(0, i);
        }
        ShowStars();
    }
    void Start()
    {
       GetComponent<AudioSource>().PlayOneShot(song, 0.5f);
        howManyLevels = levelStars.Length;
         GameMatch.stars = new float[howManyLevels];
        GetComponent<Save>().LoadScore(howManyLevels);
        ShowStars();
    }
    public void Level1()
    {
        SceneManager.LoadScene(3);
        SceneManager.LoadScene("Common",LoadSceneMode.Additive);
    }

      public void Level2()
    {
        SceneManager.LoadScene(4);
        SceneManager.LoadScene("Common",LoadSceneMode.Additive);
    }
      public void Level3()
      {
          SceneManager.LoadScene(5);
          
          SceneManager.LoadScene("Common",LoadSceneMode.Additive);
      }
      public void Level4()
      {
          SceneManager.LoadScene(6);
          SceneManager.LoadScene("Common",LoadSceneMode.Additive);
      }
      public void Level5()
      {
          SceneManager.LoadScene(7);
          SceneManager.LoadScene("Common",LoadSceneMode.Additive);
      }
      

}

