﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelControl : MonoBehaviour {

    public void Back()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
		 SceneManager.LoadScene("Common",LoadSceneMode.Additive);
    }

    public void Next()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
		 SceneManager.LoadScene("Common",LoadSceneMode.Additive);
	}
	 public void Menu()
    {
        SceneManager.LoadScene("Levels");
	}

}
