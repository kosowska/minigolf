﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftButton : MonoBehaviour {
	public GameObject lift;
	public Material redButton;
	public Material greenButton;

	// Use this for initialization
	void Start () {
		gameObject.GetComponent<Renderer>().material = redButton;
		lift.GetComponent<Animator>().speed = 0f;
	}
	
	// Update is called once per fram
	public void OnTriggerEnter(Collider _)
	{
		gameObject.GetComponent<Renderer>().material = greenButton;
		lift.GetComponent<Animator>().speed = 1.0f;

	}
	public void OnTriggerExit(Collider _)
	{
		gameObject.GetComponent<Renderer>().material = redButton;
		lift.GetComponent<Animator>().speed = 0f;
		
	}
}
