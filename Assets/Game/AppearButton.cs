﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppearButton : MonoBehaviour {

	 float appearing_speed = 2f;
  
	 void Start()
     {
         transform.localScale = new Vector3(0,0,0);
     }
 
     void Update () {
         if(Time.time > 3){
        transform.localScale =  Vector3.Lerp(transform.localScale, new Vector3(1f, 1f, 1f), 
        appearing_speed  * Time.deltaTime);
         }
     }
	
}
